import { Food } from './food';
import { Drink } from './drink';

export interface Menu{

    food: Food[];
    drinks: Drink[]; 
    docId: string;
  
}