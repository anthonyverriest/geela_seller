import { Category } from './category';
import { Product } from './product';


export interface Food{

    category: Category;
    products: Product[];
}