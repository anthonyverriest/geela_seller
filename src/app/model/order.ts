import { Product } from './product';

export interface Order{

    price: number;
    date: string;
    pickup_point: string;
    products: Product[];
    status: string;
    user_id: string;
    docId: string;
}