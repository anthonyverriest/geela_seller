import { Name } from './name';


export interface Product{

    name: Name;
    price: number;
    image: string;
    
}