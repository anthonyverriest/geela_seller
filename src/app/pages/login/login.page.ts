import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  private loginForm: FormGroup;
  private registerForm: FormGroup;

  private isLoginForm: Boolean;

  validations = {
    email: [
      { type: 'required', message: 'Email is required.' },
      { type: 'minlength', message: 'Email must be at least 6 characters long.' },
      { type: 'maxlength', message: 'Email cannot be more than 60 characters long.' },
      { type: 'pattern', message: 'Your email must contain only numbers and letters.' },
      { type: 'emailNotAvailable', message: 'Your email is already taken.' }
    ],
    password: [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 6 characters long.' },
      { type: 'maxlength', message: 'Password cannot be more than 30 characters long.' },
    ]
  };

  constructor(
    private formBuilder: FormBuilder, 
    private afAuth: AngularFireAuth,
    private router: Router
  ) { 
    this.isLoginForm = true;
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(60)
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)
      ])]
    });

    this.registerForm = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(60)
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)
      ])],
      passwordMatch: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)
      ])]
    });
  }

  async onLogin(){
    const { email, password } = this.loginForm.value;
    await this.afAuth.auth.signInWithEmailAndPassword(email, password)
    .then(msg => {
      this.router.navigateByUrl('/home');
    }, err => console.log(err));
  }

  async onRegister() {
    const { email, password, passwordMatch } = this.registerForm.value;

    if(password != passwordMatch)
      return;
    
    await this.afAuth.auth.createUserWithEmailAndPassword(email, password)
    .then(msg => {
      this.showForm();
    }, err => console.log(err));
  }

  showForm(){
    this.isLoginForm = !this.isLoginForm;
  }

}
