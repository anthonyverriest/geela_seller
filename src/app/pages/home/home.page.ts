import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss']
})
export class HomePage implements OnInit {

  private currentView: number = 0;

  private VIEW_HOME: number = 0;
  private VIEW_MENU: number = 1;
  private VIEW_ORDERS: number = 2;

  constructor(
    private afAuth: AngularFireAuth,
    private router: Router
  ){ }

  ngOnInit() {}

  async onLogout(){
    await this.afAuth.auth.signOut();
    this.router.navigateByUrl('/login').then(msg => {
      this.currentView = this.VIEW_HOME;
      location.reload()});
  }

  setView(view: number){
    this.currentView = view;
  }


}
