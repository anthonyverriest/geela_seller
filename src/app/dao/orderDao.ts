import { GenericDao } from './genericDao';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, onErrorResumeNext } from 'rxjs';
import { Injectable } from '@angular/core';
import { Order } from '../model/order';
import { COLL_ORDERS } from '../const/constants';
import { map } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
    providedIn: 'root'
})
export class OrderDao implements GenericDao<Order>{

    constructor(
        private readonly afs: AngularFirestore,
        private readonly afAuth: AngularFireAuth
    ){}

    getAll(): Observable<Order[]> {
        let user = this.afAuth.auth.currentUser;
        if(!user)
            return null;

        return this.afs.collection<Order>(COLL_ORDERS, ref => ref.where('restaurant_id', '==', user.uid))
            .valueChanges({idField: 'docId'});
    }

    update(order: Order): Promise<any>{
        return new Promise<any>((resolve, reject) => {
            this.afs.collection<Order>(COLL_ORDERS).doc(order.docId).update(order)
            .then(msg => resolve(msg), err => reject(err))
        });
    }

}