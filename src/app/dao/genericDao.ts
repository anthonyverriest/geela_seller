import { Observable } from "rxjs";

export interface GenericDao<T>{

    getAll(): Observable<T[]>;

    create?(t: T): Promise<any>;

    update?(t: T): Promise<any>;

    delete?(t: T): Promise<any>;
}

