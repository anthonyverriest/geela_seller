import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { GenericDao } from './genericDao';
import { Injectable } from '@angular/core';
import { Menu } from '../model/menu';
import { COLL_ITEMS } from '../const/constants';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
    providedIn: 'root'
})
export class ItemDao implements GenericDao<Menu>{

    constructor(
        private readonly af: AngularFirestore,
        private readonly afs: AngularFireStorage,
        private readonly afAuth: AngularFireAuth
    ){}
    
   
    getAll(): Observable<Menu[]> {
        let user = this.afAuth.auth.currentUser;
        if(!user)
            return null;

        return this.af.collection<Menu>('items', 
        ref => ref.where('restaurant_id', '==', user.uid))
        .valueChanges({idField: 'docId'});
    }

    create(menu: Menu): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.af.collection<Menu>(COLL_ITEMS).doc<Menu>(menu.docId).set(menu)
            .then(msg => resolve(msg), err => reject(err))
        });
    }

    update(menu: Menu): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.af.collection(COLL_ITEMS).doc(menu.docId).update(menu)
            .then(msg => resolve(msg), err => reject(err))
        });
    }

    delete(menu: Menu): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.af.collection(COLL_ITEMS).doc(menu.docId).delete()
            .then(msg => resolve(msg), err => reject(err))
        });
    }

    uploadToStorage(file): AngularFireUploadTask{
        const path = `${file}_${new Date().getTime()}`;
        //return this.afs.ref('items').put(info);

        const fileRef = this.afs.ref(path);
        this.afs.upload(path, file);
        return null;
        //return fileRef.getDownloadURL();

        /*task.snapshotChanges().pipe(finalize(() => {
            let uploadedFileUrl = fileRef.getDownloadURL();

            uploadedFileUrl.subscribe(res => {

            });
        }));*/
    }

}


