import { Injectable } from '@angular/core';
import { OrderRepository } from '../repository/orderRepository';
import { Order } from '../model/order';
import { map } from "rxjs/operators";


@Injectable({
    providedIn: 'root'
})
export class OrderViewModel {

    private incomingOrders: Order[];
    private inPreparationOrders: Order[];

    //Order status
    private readonly ORDERED = "ordered";
    private readonly IN_PREPARATION = "in_preparation";
    private readonly FINISHED = "finished";

    constructor(
        private orderRepository: OrderRepository
    ){

        let incOrders = this.orderRepository.getAll();
        if(incOrders != null){
            incOrders.pipe<Order[]>(map(orders => {
                return orders.filter(order => order.status===this.ORDERED)
            })).subscribe(orders => this.incomingOrders=orders);
        }else{
            this.incomingOrders = [];
        }

        let inpOrders = this.orderRepository.getAll();
        if(inpOrders != null){
            inpOrders.pipe<Order[]>(map(orders => {
                return orders.filter(order => order.status===this.IN_PREPARATION);
            })).subscribe(orders => this.inPreparationOrders=orders);
        }else{
            this.inPreparationOrders = [];
        }

    }

    getAllIncomingOrders(): Order[]{
        return this.incomingOrders;
    }

    getAllInPreparationOrders(): Order[]{
        return this.inPreparationOrders;
    }

    setOrderToInPreparation(index: number){
        let order = this.incomingOrders[index];
        order.status = this.IN_PREPARATION;

        this.orderRepository.update(order);
    }

    setOrderToFinished(index: number){
        let order = this.inPreparationOrders[index];
        order.status = this.FINISHED;

        this.orderRepository.update(order);
    }
}