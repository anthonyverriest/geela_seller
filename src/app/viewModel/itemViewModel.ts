import { ItemRepository } from '../repository/itemRepository';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Menu } from '../model/menu';

@Injectable({
    providedIn: 'root'
})
export class ItemViewModel {

    private items: Observable<Menu[]>; 

    constructor(
        private itemRepository: ItemRepository
    ){
        this.items = itemRepository.getAll();
    }

    getAllItems(): Observable<Menu[]>{
        return this.items;
    }

    create(item: Menu){ 
        return this.itemRepository.create(item);
    }

    delete(item: Menu){
        return this.itemRepository.delete(item);
    }

    upload(img){
        return this.itemRepository.uploadToStorage(img);
    }
}