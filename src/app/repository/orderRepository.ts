import { OrderDao } from '../dao/orderDao';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from '../model/order';
import { GenericRepository } from './genericRepository';

@Injectable({
    providedIn: 'root'
})
export class OrderRepository implements GenericRepository<Order>{

    private orders: Observable<Order[]>;

    constructor(
        private orderDao: OrderDao
    ){
        this.orders = orderDao.getAll();
    }

    getAll(): Observable<Order[]>{
        return this.orders;
    }

    async update(order: Order): Promise<any>{
        return this.orderDao.update(order);
    }
}