import { ItemDao } from '../dao/itemDao';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { GenericRepository } from './genericRepository';
import { Menu } from '../model/menu';

@Injectable({
    providedIn: 'root'
})
export class ItemRepository implements GenericRepository<Menu> {

    private items: Observable<Menu[]>; 

    constructor(
        private itemDao: ItemDao
    ){
        this.items = this.itemDao.getAll();
    }

    getAll(): Observable<Menu[]>{
        return this.items;
    }

    async create(menu: Menu): Promise<any>{
        return this.itemDao.create(menu);
    }

    async delete(menu: Menu): Promise<any>{
        return this.itemDao.delete(menu);
    }

    async uploadToStorage(upload): Promise<any>{
        return this.uploadToStorage(upload);
    }
    
}



