import { Observable } from "rxjs";

export interface GenericRepository<T>{

    getAll(): Observable<T[]>;
    
    create?(t: T): Promise<any>;

    delete?(t: T): Promise<any>;
}