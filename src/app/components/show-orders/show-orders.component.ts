import { Component, OnInit } from '@angular/core';
import { OrderViewModel } from 'src/app/viewModel/orderViewModel';
import { Order } from 'src/app/model/order';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-show-orders',
  templateUrl: './show-orders.component.html',
  styleUrls: ['./show-orders.component.scss'],
})
export class ShowOrdersComponent implements OnInit {

  constructor(
    private orderViewModel: OrderViewModel
  ) { }

  ngOnInit() {}

  showIncomingOrders(): Order[]{   
    return this.orderViewModel.getAllIncomingOrders();     
  }

  showInPreparationOrders(): Order[]{
    return this.orderViewModel.getAllInPreparationOrders();
  }

  onInPreparationDrop(event: CdkDragDrop<string[]>){
    if(event.previousContainer != event.container)
      this.orderViewModel.setOrderToInPreparation(event.currentIndex);
  }

  onFinishedDrop(event: CdkDragDrop<string[]>){
    if(event.previousContainer != event.container)
      this.orderViewModel.setOrderToFinished(event.currentIndex);
  }

}
