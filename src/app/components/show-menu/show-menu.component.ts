import { Component, OnInit } from '@angular/core';
import { ItemViewModel } from 'src/app/viewModel/itemViewModel';
import { Observable } from 'rxjs';
import { Menu } from 'src/app/model/menu';
import { OrderViewModel } from 'src/app/viewModel/orderViewModel';
import { Order } from 'src/app/model/order';

@Component({
  selector: 'app-show-menu',
  templateUrl: './show-menu.component.html',
  styleUrls: ['./show-menu.component.scss'],
})
export class ShowMenuComponent implements OnInit {

  private currentView: number = 0;

  private readonly VIEW_MENU = 0;
  private readonly VIEW_ADD_ITEM = 1;

  constructor(
    private itemViewModel: ItemViewModel
  ) { }

  ngOnInit() {}

  showMenu(): Observable<Menu[]>{
    return this.itemViewModel.getAllItems();
  }

  setView(view: number){
    this.currentView = view;
  }

}
