import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { AddItemComponent } from './add-item/add-item.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ShowMenuComponent } from './show-menu/show-menu.component';
import { ShowOrdersComponent } from './show-orders/show-orders.component';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
    imports: [
      IonicModule, 
      ReactiveFormsModule,
      CommonModule,
      DragDropModule
    ],
    declarations: [AddItemComponent, ShowMenuComponent, ShowOrdersComponent],
    exports: [AddItemComponent, ShowMenuComponent, ShowOrdersComponent]
  })
  export class ComponentsModule{}