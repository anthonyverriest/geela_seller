import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ItemViewModel } from 'src/app/viewModel/itemViewModel';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {

  private addItemForm: FormGroup;
  private category: string;

  private currentForm: number = 0;
  
  private readonly DRINK_FORM: number = 0;
  private readonly FOOD_FORM: number = 1;

  constructor(
    private formBuilder: FormBuilder,
    private itemViewModel: ItemViewModel
  ){}

  ngOnInit() {
    this.addItemForm = this.formBuilder.group({
      drinkName: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(30)
      ])],
      foodName: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(30)
      ])],
      foodPrice: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(30)
      ])],
      drinkPrice: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(30)
      ])],
    });
  }

  setForm(form: number){
    this.currentForm = form;
  }

  addFood(){
    let food = {
      name: this.addItemForm['foodName'],
      category: this.category
    }
    console.log(food);
  }

  setCategory(category: string){
    this.category = category;console.log(category);
  }

  uploadImage(){
    this.itemViewModel.upload('t');
  }

}
